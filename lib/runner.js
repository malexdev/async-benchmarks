'use strict'
const Promise = require('bluebird')
const chalk = require('chalk')

// define the number of times the test runner re-runs the test
const kTestBatches = 10

// this function is passed to each test to generate their test report
function reportTest(start, iterations) {
  const end = new Date() // the time that the test ended
  const duration = end.getTime() - start.getTime() // total test duration
  const opms = iterations / duration // operations per millisecond
  return { start, end, duration, opms }
}

// given an array of result objects, average the duration and opms values
function smoothResults(results) {
  // console.log('Smoothing results: %j', results)
  const count = results.length

  // validate assumptions needed to smooth the data
  if (count === 0) { throw new Error('No results passed to smooth') }
  if (count === 1) { return results[0] }

  // reduce the results down to a single total result
  const total = results.reduce((prev, current) => {
    prev.duration += current.duration
    prev.opms += current.opms
    return prev
  }, { duration: 0, opms: 0 })

  // return the averaged results
  return {
    duration: (total.duration / count).toFixed(2),
    opms: (total.opms / count).toFixed(2)
  }

}

// generate an array with the given amount of indexes
// prefilled with the value "false"
// useful for mapping
function resultsArray(batches) {
  return (new Array(batches)).fill(false)
}

// each test gets run multiple times, then are averaged out
// this way we can try to account for variances
function runTest(fn, iterations) {
  return Promise
    .mapSeries(resultsArray(kTestBatches), (item, idx) => {
      reportBatch(idx + 1)
      return fn(iterations, reportTest)
    })
    .then(smoothResults)
    .then(results => {
      clearConsoleLine()
      return results
    })
}

// report the batch of iterations that is currently being run
const reportBatch = throttle(batch => {
  clearConsoleLine()
  process.stdout.write(chalk.grey(` Batch ${batch} / ${kTestBatches}`))
}, 50)

// clear the current console line
function clearConsoleLine() {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
}

// simple throttle function.
// allows calls to fn to occur every x ms. 
// returns undefined if function is throttled.
function throttle(fn, ms) {
  let wait = false
  return (...args) => {
    if (wait) { return }
    fn(...args)
    wait = true
    setTimeout(() => wait = false, ms)
  }
}

module.exports = runTest