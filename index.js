'use strict'
const Promise = require('bluebird')
const chalk = require('chalk')
const argv = require('minimist-argv')

// require the test runner
const kIterations = argv.iterations || argv.i || 100000
const runner = require('./lib/runner')

// require in the tests
const tests = {
  cb: require('./tests/callback'),
  cbAsync: require('./tests/callback-async'),
  promise: require('./tests/promise'),
  promiseBluebird: require('./tests/promise-bluebird'),
  generator: require('./tests/generator'),
  generatorBluebird: require('./tests/generator-bluebird'),
  generatorCo: require('./tests/generator-co')
}

// report to the user which test(s) are being run
const testKeys = Object.keys(tests)
console.log('\nPending test%s: "%s". %d iterations per test.\n', testKeys.length > 1 ? 's' : '', testKeys.join(', '), kIterations)

// run the tests
Promise.mapSeries(testKeys, test => {
  console.log('Running test "%s"...', test)
  return runner(tests[test], kIterations)
  .then(result => {
    console.log(chalk.green(' Average Duration: %s'), result.duration)
    console.log(chalk.green(' Average ops / ms: %s'), result.opms)
  })
})
