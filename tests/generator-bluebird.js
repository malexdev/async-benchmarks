'use strict'
const BB = require('bluebird')

function test(iterations, reportTest) {
  const start = new Date()
  let remaining = iterations

  const tick = () => new Promise(resolve => process.nextTick(resolve))

  const doTest = BB.coroutine(function* () {
    while(--remaining !== 0) { yield tick() }
  })

  return doTest().then(() => reportTest(start, iterations))
}

module.exports = test