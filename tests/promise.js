'use strict'

function test(iterations, reportTest) {
  const start = new Date()
  
  const doTest = () => {
    let remaining = iterations

    const testRunner = () => new Promise(resolve => process.nextTick(resolve))
    return pwhile(() => --remaining !== 0, testRunner)
  }

  return doTest().then(() => reportTest(start, iterations))
}

// simple promise while implementation
function pwhile(check, fn) {
  return new Promise(resolve => {
    
    const loop = () => {
      if (!check()) { return resolve() } 
      return fn().then(loop)
    }

    process.nextTick(loop)
  })
}

module.exports = test