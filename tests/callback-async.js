'use strict'

const async = require('async')

function test(iterations, reportTest) {
  const start = new Date()

  const doTest = (done) => {
    let remaining = iterations
    
    async.whilst(
      () => --remaining !== 0, // iteration check
      cb => process.nextTick(cb), // iteration 
      done // finished
    )
  }

  return new Promise(done => doTest(done)).then(() => reportTest(start, iterations))
}

module.exports = test