'use strict'

function test(iterations, reportTest) {
  const start = new Date()

  const tick = thunkify(process.nextTick)
  
  const doTest = (done) => {
    let remaining = iterations

    run(function* () {
      while(--remaining !== 0) { yield tick() }
      done()
    })

  }

  return new Promise(done => doTest(done)).then(() => reportTest(start, iterations))
}

// convert a node function into a thunk (callable without passing a callback)
function thunkify (fn) {
  return (...args) => cb => fn(...args, cb)
}

// wrapper to run a generator in a style used for resolving async thunks
function run(fn) {
  const gen = fn() // suspend the generator

  // this is our generator wrapper
  // it's called every time a thunk completes
  const next = (err, val) => {
    if (err) { return gen.throw(err) } // throw if an error...
    
    const cont = gen.next(val) // run the generator
    if (cont.done) { return } // if the generator is tapped out, stop

    const cb = cont.value // get the return value for the previous generator
    cb(next) // run the callback
  }

  next() // run next for the first time
}

module.exports = test