'use strict'

function test(iterations, reportTest) {
  const start = new Date()

  const doTest = (done) => {
    let remaining = iterations
    const testRunner = callback => process.nextTick(callback)
    const cb = () => { --remaining === 0 ? done() : testRunner(cb) }
    testRunner(cb)
  }

  return new Promise(done => doTest(done)).then(() => reportTest(start, iterations))
}

module.exports = test