'use strict'
const co = require('co')

function test(iterations, reportTest) {
  const start = new Date()
  let remaining = iterations

  const tick = () => new Promise(resolve => process.nextTick(resolve))

  const doTest = co.wrap(function* () {
    while(--remaining !== 0) { yield tick() }
  })

  return doTest().then(() => reportTest(start, iterations))
}

module.exports = test