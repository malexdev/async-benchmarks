# async-benchmark

This project benchmarks various async methods of writing node software.

#### Callbacks (native)

To be determined

#### Callbacks (Async.js)

To be determined

#### Promises (native)

To be determined

#### Promises (Bluebird)

To be determined

#### Generators (native)

To be determined

#### Generators (co)

To be determined

#### Generators (Bluebird Promise.coroutine)

To be determined

#### Babel async/await (generator backed)

To be determined